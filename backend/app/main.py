import requests
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.get("/upcoming")
def get_upcoming():
    results = requests.get("https://api.spacexdata.com/v5/launches/upcoming")
    return results.json()

@app.get("/launch/{launch_id}")
def get_laucnh_by_id(launch_id: str):
    results = requests.get("https://api.spacexdata.com/v5/launches/"+launch_id)
    return results.json()
